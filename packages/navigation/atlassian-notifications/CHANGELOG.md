# @atlaskit/atlassian-notifications

## 0.1.2

### Patch Changes

- [patch][e1dc937728](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e1dc937728):

  Adding a private \_url prop for notifications to enable testing/examples

## 0.1.1

### Patch Changes

- [patch][5eb3d1fc75](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5eb3d1fc75):

  Removed spinner from the notifications package (handled by the iframe content instead)
